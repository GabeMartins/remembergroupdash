import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:54596/api",
});

export default api;
