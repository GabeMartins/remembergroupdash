import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
    Link
} from "react-router-dom";
import { ReactComponent as LogoIcon } from "../assets/svgs/logoIcon.svg";
import { ReactComponent as HomeIcon } from "../assets/svgs/homeIcon.svg";
import { ReactComponent as SearchIcon } from "../assets/svgs/searchIcon.svg";
import { ReactComponent as ReportingIcon } from "../assets/svgs/reportingIcon.svg";
import { ReactComponent as MergeMembersIcon } from "../assets/svgs/mergeMembersIcon.svg";
import { ReactComponent as TransferPointsIcon } from "../assets/svgs/transferPointsIcon.svg";
import { ReactComponent as SettingsIcon } from "../assets/svgs/settingsIcon.svg";
import { ReactComponent as ManageUsersIcon } from "../assets/svgs/manageUsersIcon.svg";
import { ReactComponent as EloopIcon } from "../assets/svgs/eloopIcon.svg";
import { ReactComponent as VideoIcon } from "../assets/svgs/videoIcon.svg";

const Sidebar = styled.div`
  display: flex;
  flex: 2;
  background-color: #00334d;
  flex-direction: column;
`;

const LinkContainer = styled(Link)`
  color: #fff;
  outline: none;
  text-decoration: none;
  border-top: 1px solid #616161;
  padding: 22px;
  background-color: ${(props) => (props.selected ? "#11b5e9" : "#00334d")};
  &:hover {
    background-color: #11b5e9;
  }
`;

const LogoBrand = styled.div`
  margin-left: 44px;
  margin-top: 48px;
  margin-bottom: 50px;
`;

const LabelLink = styled.label`
  margin-left: 12px;
  /* font-family: "Open Sans";*/
  font-size: 20px;
`;

const LinkContent = styled.span`
  margin-left: 44px;
`;

export default function NavBar(props) {

    let style = {
        marginLeft: '0em',
        zIndex: '99991'
    }

    if(props.mobileMode){
        style = {
            ...style,
            position: 'absolute',
            marginTop: '5em',
            width: '150%'
        }
    }

    if (props.showMenu){
        style = {
            ...style,
            marginLeft: '-40em',
            width: '100%'
        }
    }

    return (
        <Sidebar style={{...style}}>
            {(props.mobileMode) ? null : (
            <LogoBrand>
                <LogoIcon />
            </LogoBrand>)}
          
            <LinkContainer
                selected={
                    window.location.pathname.includes("dashboard") ||
                    window.location.pathname == "/admin"
                }
                to="/admin/dashboard"
            >
                <HomeIcon />
                <LabelLink>Dashboard</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("findmembers")}
                to="/admin/findmembers"
            >
                <SearchIcon />
                <LabelLink>Find Member</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("reporting")}
                to="/admin/reporting"
            >
                <ReportingIcon />
                <LabelLink>Reporting</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("mergemembers")}
                to="/admin/mergemembers"
            >
                <MergeMembersIcon />
                <LabelLink>Merge Members</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("transferpoints")}
                to="/admin/transferpoints"
            >
                <TransferPointsIcon />
                <LabelLink>Transfer Points</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("settings")}
                to="/admin/settings"
            >
                <SettingsIcon />
                <LabelLink>Settings</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("manageusers")}
                to="/admin/manageusers"
            >
                <ManageUsersIcon />
                <LabelLink>Manage Users</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("eloop")}
                to="/admin/eloop"
            >
                <EloopIcon />
                <LabelLink>eLoop</LabelLink>
            </LinkContainer>
            <LinkContainer
                selected={window.location.pathname.includes("trainingvideos")}
                to="/admin/trainingvideos"
            >
                <VideoIcon />
                <LabelLink>Training Videos</LabelLink>
            </LinkContainer>
        </Sidebar>)
}

