import React, { useState } from "react";
import styled from "styled-components";
import DashboardScreen from "../screens/DashboarScreen";
import FindMember from "../screens/FindMember";
import Settings from "../screens/Settings";
import MenuIcon from "../assets/svgs/menu.png";
import RememberIcon from "../assets/svgs/remember.png";
import {
  useRouteMatch,
  Switch,
  Route,
  useLocation,
} from "react-router-dom";
import NavBar from './NavBar'
import { ReactComponent as I25KiaIcon } from "../assets/svgs/i25KiaIcon.svg";
import Profile from "../assets/svgs/profile.png";
import LandingPage from "../screens/LandingPage";
import Reporting from "../screens/Reporting";
import MergeMembers from "../screens/MergeMembers";
import TransferPoints from "../screens/TransferPoints";
import ManageUsers from '../screens/ManageUsers'
import ManagePoints from '../screens/ManagePoints'
import EditProfile from "../screens/EditProfile";

const Container = styled.div`
  display: flex;
  flex: 1;
  background-color: #fff;
  min-height: 100vh;
`;

const Header = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-between;
  background: #f6f8f9;
  max-height: 132px;
`;

const RightSide = styled.div`
  display: flex;
  flex: 8;
  flex-direction: column;
`;

const Content = styled.div`
  display: flex;
  background-color: #fff;
`;


const HeaderLeft = styled.div`
  margin-top: 13px;
  margin-left: 66px;
  display: flex;
`;

const HeaderRight = styled.div`
  margin-top: 44px;
  margin-right: 44px;
  flex: 1;
  justify-content: end;
  display: flex;
`;

const WelcomeContainer = styled.div`
  /*font-family: "Open Sans";*/
  color: #232323;
  font-size: 16px;
  display: flex;
  flex-direction: column;
  margin-right: 12px;
`;

const WelcomeText = styled.span`
  display: flex;
`;

const LogOutLink = styled.a`
  display: flex;
  justify-content: flex-end;
`;

const ProfileImage = styled.img`
  height: 44px;
  width: 44px;
  border-radius: 50%;
`;

export default function Dashboard() {
  let match = useRouteMatch();
  const location = useLocation();

  const [showMenu, setShowMenu] = useState(false)

  const width = window.innerWidth
  let mobileMode = false
  if(width < 800){
      mobileMode = true
  }

  return (
    <Container>
      <NavBar mobileMode={mobileMode} showMenu={showMenu} />
     
      <RightSide>

        {(mobileMode) ? ( <div style={{
          height: '5em',
          width: '100%',
          backgroundColor: '#00334D'
        }}>

          <img onClick={() => {
            setShowMenu(!showMenu)
          }} width="50" style={{
            marginLeft: '1em',
            marginTop: '1em',
            cursor: 'pointer'
          }} src={MenuIcon} />


          <img width="100" style={{
            marginLeft: '4em'
          }} src={RememberIcon} />

        <img width="40" style={{
            marginLeft: '3em'
          }} src={Profile} />
        </div>) : null}
       

        <Header>
          <HeaderLeft>
            <I25KiaIcon />
          </HeaderLeft>
          <HeaderRight>
            <WelcomeContainer>
              <WelcomeText>Welcome, Admin</WelcomeText>
              <LogOutLink>Log out</LogOutLink>
            </WelcomeContainer>
            <ProfileImage src={Profile} />
          </HeaderRight>
        </Header>
        <Content>
          <Switch>
            <Route
              path={`${match.path}/transferpoints`}
              component={TransferPoints}
            />
            <Route
              path={`${match.path}/mergemembers`}
              component={MergeMembers}
            />
            <Route path={`${match.path}/reporting`} component={Reporting} />
            <Route path={`${match.path}/findmembers`} component={FindMember} />
            <Route path={`${match.path}/settings`} component={Settings} />
            <Route path={`${match.path}/manageusers`} component={ManageUsers} />
            <Route path={`${match.path}/managepoints`} component={ManagePoints} />
            <Route path={`${match.path}/editprofile`} component={EditProfile} />
            <Route
              path={`${match.path}/dashboard`}
              component={DashboardScreen}
            />
            <Route path={`${match.path}`} component={DashboardScreen} />
          </Switch>
        </Content>
      </RightSide>
    </Container>
  );
}
