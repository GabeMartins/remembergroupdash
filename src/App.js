import { BrowserRouter, Switch, Route } from "react-router-dom";
import Dashboard from "./layouts/Dashboard";
import LandingPage from "./screens/LandingPage";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/admin" component={Dashboard} />
        <Route path="" component={LandingPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
