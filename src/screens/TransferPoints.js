import React from "react";
import styled from "styled-components";
import TransferPointsForm from "../components/TransferPointsForm";
import TransferPointsNote from "../components/TransferPointsNote";

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function TransferPoints() {
  return (
    <Container>
      <Row>
        <TransferPointsForm />
        <TransferPointsNote />
      </Row>
    </Container>
  );
}
