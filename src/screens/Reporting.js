import React from "react";
import styled from "styled-components";
import ReportingForm from "../components/ReportingForm";
import ReportingNote from "../components/ReportingNote";

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function Reporting() {
  return (

      <Row>
        <ReportingForm />
        <ReportingNote />
      </Row>
 
  );
}
