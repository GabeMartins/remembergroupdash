import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Summaries from "../components/Summaries";
import Totals from "../components/Totals";
import Points from "../components/Points";
import NumberRedemptions from "../components/NumberRedemptions";
import api from "../services/api";

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function DashboardScreen() {
  const [pointsAwarded, setPointsAwarded] = useState([]);
  const [pointsRedeemed, setPointsRedeemed] = useState([]);
  useEffect(async () => {
    // const response = await api.get("/sumaries", {
    //   params: {
    //     programPK: 111,
    //   },
    // });
    // setPointsAwarded(response.data.pointsAwarded);
    // setPointsRedeemed(response.data.pointsRedeemed);
  }, []);

  return (
    <Container>
      <Row>
        <Summaries
          pointsAwarded={pointsAwarded}
          pointsRedeemed={pointsRedeemed}
        />
        <Totals pointsRedemptions="88,345" thisDate="745" />
      </Row>
      <Row>
        <Points pointsAwarded={pointsAwarded} pointsRedeemed={pointsRedeemed} />
        <NumberRedemptions />
      </Row>
    </Container>
  );
}
