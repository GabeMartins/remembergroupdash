import React from "react";
import styled from "styled-components";
import MergeMembersForm from "../components/MergeMembersForm";
import MergeMembersNote from "../components/MergeMembersNote";

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function MergeMembers() {
  return (
    <Container>
      <Row>
        <MergeMembersForm />
        <MergeMembersNote />
      </Row>
    </Container>
  );
}
