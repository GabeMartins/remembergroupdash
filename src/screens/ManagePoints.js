import React, { useState, useEffect } from "react";
import styled from "styled-components";
import ManagePointsFormOne from "../components/ManagePointsFormOne";
import ManagePointsFormTwo from '../components/ManagePointsFormTwo'
import ManagePointsFormThree from '../components/ManagePointsFormThree'
import ManagePointsNote from "../components/ManagePointsNote";
import ManagePointsTable from "../components/ManagePointsTable";


const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function ManagePoints() {

  const [i, setI] = useState(0)
  let timeout

  useEffect(() => {
    timeout = setInterval(() => {
      if(i < 2){
        setI(i+1)
      }
    }, 5000);
  })

  if(i == 3){
    clearInterval(timeout)
  }

  const managePointsForm = () => {
    const components = [
      <ManagePointsFormOne />,
      <ManagePointsFormTwo />,
      <ManagePointsFormThree />,
    ]

    return components[i]
  }


  return (
    <Container>
      <Row>
        {managePointsForm()}
        <ManagePointsNote />
      </Row>
      <Row>
        <ManagePointsTable />
      </Row>
    </Container>
  );
}
