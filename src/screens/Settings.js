import React from "react";
import styled from "styled-components";
import ChangePasswordForm from '../components/ChangePasswordForm'
import ChangePasswordNote from "../components/ChangePasswordNote";

const Title = styled.h1`
  font-weight: 600;
  font-size: 40px;
  color: #11b5e9;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function Settings() {
  return (
    <Container>
      <Row><Title>Settings</Title></Row>
      <Row>
        <ChangePasswordForm />
        <ChangePasswordNote />
      </Row>
    </Container>
  );
}
