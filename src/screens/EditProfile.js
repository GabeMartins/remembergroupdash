import React, { useState, useEffect } from "react";
import styled from "styled-components";
import EditProfileFormOne from "../components/EditProfileFormOne";
import EditProfileFormThree from "../components/EditProfileFormThree";
import EditProfileFormTwo from "../components/EditProfileFormTwo";
import EditProfileNote from "../components/EditProfileNote";
import ManagePointsFormOne from "../components/ManagePointsFormOne";
// import ManagePointsFormTwo from '../components/ManagePointsFormTwo'
// import ManagePointsFormThree from '../components/ManagePointsFormThree'
import ManagePointsNote from "../components/ManagePointsNote";
import ManagePointsTable from "../components/ManagePointsTable";


const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function EditProfile() {

  const [i, setI] = useState(0)
  let timeout

  // useEffect(() => {
  //   timeout = setInterval(() => {
  //     if(i < 2){
  //       setI(i+1)
  //     }
  //   }, 5000);
  // })

  // if(i == 2){
  //   clearInterval(timeout)
  // }

  const editProfileForm = () => {
    const components = [
      <EditProfileFormOne />,
      <EditProfileFormTwo />,
      <EditProfileFormThree />
    ]

    return components[0]
  }

  return (
    <Container>
      <Row>
        {editProfileForm()}
        <EditProfileNote />
      </Row>

    </Container>
  );
}
