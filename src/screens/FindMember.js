import React, { useState } from "react";
import styled from "styled-components";
import FindMemberForm from "../components/FindMemberForm";
import FindMemberNote from "../components/FindMemberNote";
import FindMembersTable from "../components/FindMembersTable";
import api from "../services/api";

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;
  
  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default function FindMember() {
  const [members, setMembers] = useState([]);
  async function onSearch(data) {
    const response = await api.get("/members", {
      params: {
        memberNumber: data.number,
        customerId: data.customerId,
        lastName: data.lastName,
        homePhone: data.phone,
        emailAdress: data.email,
      },
    });
    setMembers(response.data);
  }

  return (
    <Container>
      <Row>
        <FindMemberForm onSearch={onSearch} />
        <FindMemberNote />
      </Row>
      <Row>
        <FindMembersTable members={members} />
      </Row>
    </Container>
  );
}
