import React, { useEffect } from "react";
import styled from "styled-components";
import {
  Link,
  useHistory,
  useRouteMatch,
  Switch,
  Route,
  useLocation,
} from "react-router-dom";
import { ReactComponent as Remember } from "../assets/svgs/remember.svg";
import Dashboard from "../layouts/Dashboard";

const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  background-color: #00334d;
  min-height: 100vh;
`;

const LinkContent = styled(Link)`
  margin-top: 398px;
`;

export default function LandingPage() {
  let history = useHistory();
  useEffect(() => {
    setTimeout(function () {
      history.push("/admin/dashboard");
    }, 5000);
  }, []);
  return (
    <Container>
      <LinkContent to="/admin/dashboard">
        <Remember />
      </LinkContent>
    </Container>
  );
}
