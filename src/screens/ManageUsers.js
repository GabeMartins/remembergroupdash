import React, { useState } from "react";
import styled from "styled-components";
import UsersList from '../components/UsersList'
import AddUserForm from '../components/addUser'

const Title = styled.h1`
  font-weight: 600;
  font-size: 40px;
  color: #11b5e9;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex: 1;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

const users = [
  {
    id: '1',
    name: 'Gabe Martins',
    email: 'gabemartins@fetch.ly',
    active: true
  },
  {
    id: '2',
    name: 'Gustavo',
    email: 'gustavoc@fetch.ly',
    active: true
  },
  {
    id: '3',
    name: 'Gabe',
    email: 'gabe@fetch.ly',
    active: false
  },
  {
    id: '4',
    name: 'Marina',
    email: 'marina@fetch.ly',
    active: true
  }
]

export default function ManageUsers() {

  const [userFormStatus, setUserFormStatus] = useState('hide')
  const [currentUser, setCurrentUser] = useState({})


  const openUserRegister = user => {
    setUserFormStatus('edit')
    setCurrentUser({user})
  }

  const openAddUserRegister = () => {
    setUserFormStatus('add')
    setCurrentUser({})
  }

  const closesAddUserRegister = () => {
    setUserFormStatus('hide')
  }


  return (
    <Container>
      <Row>
        <Title>Manage Users</Title>  
      </Row>
      <Row>
        <UsersList openNewUser={() => openAddUserRegister()} openUser={(user) => openUserRegister(user)} users={users} />
        <AddUserForm close={() => closesAddUserRegister()} status={userFormStatus} user={currentUser} />
      </Row>
    </Container>
  );
}
