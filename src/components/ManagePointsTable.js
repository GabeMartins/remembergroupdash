import React from "react";
import styled from "styled-components";
import searchIcon from '../assets/svgs/search-icon.svg'

const EditIcon = styled.i`
  float: right;
  color: #11B5E9;
  cursor: pointer;
  border: 1px solid #11B5E9;
  padding: 5px;
  border-radius: 50%;
`

const Row = styled.div`
  display: flex;
  flex: 1;
  margin-bottom: 2em;
`;


const Container = styled.div`
  height: auto;
  width: 100%;
  background-color: white;
  border-radius: 25px;
  padding: 22px;
  padding-top: 10px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;

const Table = styled.table` 
    width: 100%;
    display: table;
    border-collapse: collapse;
    border-spacing: 0;
`

const Title = styled.h1`
  font-weight: 600;
  font-size: 40px;
  color: #11b5e9;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Td = styled.td`
    line-height: 1.5;
    font-size: 12px;
    min-width: 10em;
    padding-bottom: 1em;
`

const Block = styled.div` 
    width: 50%;
    height: 3em;
    display: block;
`

const PRight = styled.p`
    font-size: 16px;
    color: #11b5e9;
    text-align: right;

`

export default function ManagePointsTable() {
    const fields = [
        {
            fieldName: 'date',
            label: 'DATE'
        },
        {
            fieldName: 'orderId',
            label: 'ORDER ID'
        },
        {
            fieldName: 'partner',
            label: 'PARTNER'
        },
        {
            fieldName: 'location',
            label: 'LOCATION'
        },
        {
            fieldName: 'points',
            label: 'POINTS'
        },
        {
            fieldName: 'description',
            label: 'DESCRIPTION'
        },

    ]

    const data = [
        {
            date: '5/16/2020',
            orderId: 'Redemption',
            partner: 'OpenRoad Auto Group',
            location: 'OpenRoad Hyundai',
            points: '1982',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor venenatis, nam vel habitant eget quam quis vulputate. In feugiat donec amet fermentum dignissim laoreet. Vel rhoncus maecenas et convallis. Viverra mattis enim bibendum tellus sed id.'

        },
        {
            date: '5/16/2020',
            orderId: 'Redemption',
            partner: 'OpenRoad Auto Group',
            location: 'OpenRoad Hyundai',
            points: '1982',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor venenatis, nam vel habitant eget quam quis vulputate. In feugiat donec amet fermentum dignissim laoreet. Vel rhoncus maecenas et convallis. Viverra mattis enim bibendum tellus sed id.'

        },
        {
            date: '5/16/2020',
            orderId: 'Redemption',
            partner: 'OpenRoad Auto Group',
            location: 'OpenRoad Hyundai',
            points: '1982',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor venenatis, nam vel habitant eget quam quis vulputate. In feugiat donec amet fermentum dignissim laoreet. Vel rhoncus maecenas et convallis. Viverra mattis enim bibendum tellus sed id.'

        },
    ]

    const renderFields = () => {
        return fields.map(field => <th>{field.label}</th>)
    }

    const renderBodyLines = () => {
        return data.map(d => {
            let line = fields.map(field => {
                if(field.fieldName == 'edit'){
                   return <td><EditIcon  onClick={e => {
                    e.preventDefault()
                  }} className="material-icons">edit</EditIcon></td>
                }

                if(field.fieldName == 'manage'){
                    return <td><EditIcon  onClick={e => {
                        e.preventDefault()
                      }} className="material-icons">access_time</EditIcon></td>
                }

                return <Td>{d[field.fieldName]}</Td>
            })
            return (<tr>{line}</tr>)
        })
    }

    return (<Container>
        <Row>
            <Block>
                <Title>Aaron Johnson<span style={{
                    fontSize: '16px',
                    color: '#616161',
                    float: 'none'
                }}>(123456789)</span></Title>
                
            </Block>

            <Block style={{
                float: 'right'
            }}>
                <PRight>0 Available Points</PRight>
                <PRight style={{
                     color: '#616161',
                }}>Worth $0</PRight>
                
            </Block>


            
            

        </Row>
        
        <Table>
            <thead style={{
                color: '#616161',
                borderBottom: '1px solid #D6D6D6',
                lineHeight: '37px',
            }}>
                <tr> {renderFields()}</tr>
             
            </thead>

            <tbody style={{
                color: 'rgba(0,0,0,0.87)',
                lineHeight: '3',
                fontSize: '16px'
                }}>
              {renderBodyLines()}
            </tbody>
        </Table>
    </Container>)
}