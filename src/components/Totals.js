import React from "react";
import styled from "styled-components";

const Container = styled.div`
  height: 173px;
  width: 376px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat;*/
  margin: 10px;
`;

const TitleContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-top: 1px;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  font-size: 40px;
  font-weight: 600;
  color: #232323;
  flex-direction: column;
`;

const ItemCount = styled.div`
  display: flex;
  flex: 1;
  font-size: 40px;
  font-weight: 600;
  color: #11b5e9;
  flex-direction: column;
`;

const Label = styled.span`
  /*font-family: "Open Sans";*/
  font-size: 16px;
  margin-top: 4px;
  color: #232323;
`;

const LabelTitle = styled.h1`
  font-size: 16px;
  color: #616161;
  font-weight: 600;
  display: flex;
  flex: 1;
`;

export default function Totals({ pointsRedemptions, thisDate }) {
  return (
    <Container>
      <TitleContainer>
        <LabelTitle>OCTOBER</LabelTitle>
        <LabelTitle>Mon, 21st</LabelTitle>
      </TitleContainer>
      <Divider />
      <Content>
        <Item>
          {pointsRedemptions}
          <Label>Points Redemptions</Label>
        </Item>
        <ItemCount>
          {thisDate}
          <Label>This Date</Label>
        </ItemCount>
      </Content>
    </Container>
  );
}
