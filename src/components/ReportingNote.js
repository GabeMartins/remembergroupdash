import React from "react";
import styled from "styled-components";

const Container = styled.div`
  height: 365px;
  width: 400px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;
const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.p`
  /*font-family: "Open Sans" !important;*/
  font-size: 16px;
  margin-top: 4px;
  color: #000000;
  font-weight: 400;
`;

export default function ReportingNote() {
  return (
    <Container>
      <Content>
        <Label>
          Please fill out the form for the report you would like to download. If
          a date range is required please fill in the start and end date. when
          the report completes an Excel document will begin downloading.
        </Label>
        <Label>
          If you have any questions, please contact re:member group at
          952.224.8000.
        </Label>
      </Content>
    </Container>
  );
}
