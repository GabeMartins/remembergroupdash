import React from "react";
import styled from "styled-components";
import BarGraph from "./BarGraph";

const Container = styled.div`
  width: 632px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat;*/
  margin: 10px;
`;

const Title = styled.h1`
  font-weight: 600;
  font-size: 16px;
  color: #616161;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-top: 12px;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  color: #232323;
  flex-direction: column;
  justify-content: right;
  align-items: flex-end;
  margin-right: 54px;
`;

const Label = styled.span`
  /*font-family: "Open Sans";*/
  font-size: 16px;
  margin-top: 4px;
`;

export default function NumberRedemptions() {
  return (
    <Container>
      <Title>NUMBER OF REDEMPTIONS</Title>
      <Divider />
      <Item>October</Item>
      <Content>
        <BarGraph />
      </Content>
    </Container>
  );
}
