import React from "react";
import styled from "styled-components";
import { ReactComponent as PencilIcon } from "../assets/svgs/pencilicon.svg";
import { ReactComponent as ClocklIcon } from "../assets/svgs/clockIcon.svg";

const EditIcon = styled(PencilIcon)`
  color: #11b5e9;
  cursor: pointer;
  border: 1px solid #11b5e9;
  padding: 5px;
  border-radius: 50%;
`;

const ManageIcon = styled(ClocklIcon)`
  color: #11b5e9;
  cursor: pointer;
  border: 1px solid #11b5e9;
  padding: 5px;
  border-radius: 50%;
`;

const Container = styled.div`
  height: auto;
  width: 100%;
  background-color: white;
  border-radius: 25px;
  padding: 22px;
  padding-top: 10px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;

const Table = styled.table`
  width: 100%;
  display: table;
  border-collapse: collapse;
  border-spacing: 0;
`;

const Row = styled.tr`
  text-align: left;
`;

const Item = styled.td`
  text-align: center;
`;

const ItemTitle = styled.th`
  text-align: center;
`;

export default function FindMembersTable({ members = [] }) {
  const renderBodyLines = () => {
    return members.map((member) => {
      return (
        <Row>
          <td>{member.FirstName}</td>
          <td>{member.MemberPk}</td>
          <td>{member.CustomerPk}</td>
          <td>{member.HomePhone}</td>
          <td>{member.Email}</td>
          <Item>
            <ManageIcon
              onClick={(e) => {
                e.preventDefault();
              }}
            />
          </Item>
          <Item>
            <EditIcon
              onClick={(e) => {
                e.preventDefault();
              }}
            />
          </Item>
        </Row>
      );
    });
  };

  return (
    <Container>
      <Table>
        <thead
          style={{
            color: "#616161",
            borderBottom: "1px solid #D6D6D6",
            lineHeight: "37px",
          }}
        >
          <Row>
            <th>NAME</th>
            <th>MEMBER ID</th>
            <th>CUST ID</th>
            <th>PHONE NUMBER</th>
            <th>EMAIL</th>
            <ItemTitle>MANAGE</ItemTitle>
            <ItemTitle>EDIT</ItemTitle>
          </Row>
        </thead>

        <tbody
          style={{
            color: "rgba(0,0,0,0.87)",
            lineHeight: "3",
            fontSize: "16px",
          }}
        >
          {renderBodyLines()}
        </tbody>
      </Table>
    </Container>
  );
}
