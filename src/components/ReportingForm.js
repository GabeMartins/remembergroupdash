import React from "react";
import styled from "styled-components";
import ReactSelect from "react-select";
import "../assets/css/select.css";

const Container = styled.div`
  height: auto;
  width: 380px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;

const Title = styled.h1`
  font-weight: 600;
  font-size: 40px;
  color: #11b5e9;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const ItemButton = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: right;
  align-items: flex-end;
  margin-top: 80px;
  margin-right: 22px;
`;

const Label = styled.label`
  /*font-family: "Open Sans" !important;*/
  font-size: 12px;
  color: #616161;
  font-weight: 600;
  margin-left: 15px;
  margin-bottom: 4px;
`;

const Select = styled.select`
  width: 356px;
  height: 44px;
  border: 1px solid;
  border-color: #c4c4c4;
  border-radius: 30px;
  font-size: 16px;
  background-color: #ffffff;
  padding-left: 22px;
  margin-bottom: 22px;
  ::placeholder {
    color: #cccccc;
  }
`;

const DataComboBox = styled.input`
  width: 315px;
  height: 44px;
  border: 1px solid;
  border-color: #c4c4c4;
  border-radius: 30px;
  font-size: 16px;
  background-color: #ffffff;
  padding-left: 22px;
  padding-right: 18px;
  margin-bottom: 22px;
  ::placeholder {
    color: #cccccc;
  }
  ::selection {
    background-color: blue;
  }
`;

const Button = styled.button`
  width: 208px;
  height: 44px;
  border-radius: 30px;
  background-color: #11b5e9;
  font-size: 20px;
  font-weight: 600;
  color: #ffffff;
  border: none;
  cursor: pointer;
`;

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const report_options = [
  { value: "new", label: "New Version" },
  { value: "old", label: "Old Version" },
];

export default function ReportingForm() {
  return (
    <Container>
      <Title>Reporting</Title>
      <Divider />
      <Content>
        <Item>
          <Label>REPORT TYPE</Label>
          <ReactSelect className="rSelectCustom" options={options} />
        </Item>

        <Item style={{
          marginTop: '1.5em'
        }}>
          <Label>START DATE</Label>
          <DataComboBox type="date" />
        </Item>
        <Item>
          <Label>END DATE</Label>
          <DataComboBox type="date" />
        </Item>

        <Item>
          <Label>OLD/NEW REPORT</Label>
          <ReactSelect className="rSelectCustom" options={report_options} />
        </Item>


        <ItemButton>
          <Button>Download</Button>
        </ItemButton>
      </Content>
    </Container>
  );
}
