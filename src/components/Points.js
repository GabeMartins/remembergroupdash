import React, { useEffect } from "react";
import styled from "styled-components";
import PieGraph from "./PieGraph";

const Container = styled.div`
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /* font-family: Montserrat;*/
  margin: 10px;
  height: 293px;
  width: 376px;
`;

const Title = styled.h1`
  font-weight: 600;
  font-size: 16px;
  color: #616161;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-top: 12px;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  font-size: 30px;
  font-weight: 600;
  color: #ffffff;
  flex-direction: column;
  margin-left: 12px;
  justify-content: flex-end;
`;

const ItemPie = styled.div`
  display: flex;
  flex: 1;
  font-size: 30px;
  font-weight: 600;
  color: #ffffff;
  flex-direction: column;
  align-content: flex-end;
`;

const ItemLabels = styled.div`
  display: flex;
  font-weight: 600;
  flex-direction: row;
  margin-top: 12px;
  align-items: center;
`;

const Label = styled.span`
  /*font-family: "Open Sans";*/
  font-size: 16px;
  color: #232323;
  display: flex;
`;

const CircleRedeemed = styled.span`
  border-radius: 50%;
  width: 22px;
  height: 22px;
  background: #ffc327;
  margin-left: 22px;
  margin-right: 12px;
`;

const CircleAwarded = styled.span`
  border-radius: 50%;
  width: 22px;
  height: 22px;
  background: #fd5925;
  margin-left: 22px;
  margin-right: 12px;
`;

export default function Points({ pointsAwarded, pointsRedeemed }) {
  return (
    <Container>
      <Title>POINTS</Title>
      <Divider />
      <Content>
        <ItemPie>
          <PieGraph
            pointsAwarded={pointsAwarded}
            pointsRedeemed={pointsRedeemed}
          />
        </ItemPie>

        <Item>
          <ItemLabels>
            <CircleRedeemed />
            <Label>Redeemed</Label>
          </ItemLabels>
          <ItemLabels>
            <CircleAwarded />
            <Label>Awarded</Label>
          </ItemLabels>
        </Item>
      </Content>
    </Container>
  );
}
