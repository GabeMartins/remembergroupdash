import React from "react";
import styled from "styled-components";
import Profile from "../assets/svgs/profile.png";

const Container = styled.div`
  height: auto;
  width: 400px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;


const Title = styled.h1`
  font-weight: 600;
  font-size: 16px;
  color: #616161;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const ItemButton = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: right;
  align-items: flex-end;
  margin-top: 80px;
  margin-right: 22px;
`;

const Label = styled.label`
  /*font-family: "Open Sans" !important;*/
  font-size: 12px;
  color: #616161;
  font-weight: 600;
  margin-left: 15px;
  margin-bottom: 4px;
`;

const TextBox = styled.input`
  width: 356px;
  height: 44px;
  border: 1px solid;
  border-color: #c4c4c4;
  border-radius: 30px;
  font-size: 16px;
  background-color: #ffffff;
  padding-left: 22px;
  margin-bottom: 22px;
  ::placeholder {
    color: #cccccc;
  }
`;

const Button = styled.button`
  width: 179px;
  height: 44px;
  border-radius: 30px;
  background-color: #11b5e9;
  font-size: 20px;
  font-weight: 600;
  color: #ffffff;
  border: none;
  cursor: pointer;
`;

const ProfileImage = styled.img`
  height: 70px;
  width: 70px;
  border-radius: 50%;
  margin: auto;
  margin-bottom: 1em;
`;

const ImageOutterContainer = styled.div`
  heigth: 200px;
  width: 100%;
  text-align: center;
  margin-bottom: 2em;
`

const ImageInnerContainer = styled.div`
  heigth: 200px;
  width: 50%;
  margin: auto;
  display: flex;
  flex-direction: column;
`

export default function FindMemberForm() {
  return (
    <Container>
      <Title>CHANGE PASSWORD</Title>
      <Divider />
      <Content>
        <ImageOutterContainer>
          <ImageInnerContainer>
            <ProfileImage src={Profile} />
            <Label>CHANGE PICTURE</Label>
          </ImageInnerContainer>
        </ImageOutterContainer>
        
        <Item>
          <Label>OLD PASSWORD</Label>
          <TextBox type="text" placeholder="Old password" />
        </Item>
        <Item>
          <Label>NEW PASSWORD</Label>
          <TextBox type="text" placeholder="New password" />
        </Item>
      
        <ItemButton>
          <Button>Save</Button>
        </ItemButton>
      </Content>
    </Container>
  );
}
