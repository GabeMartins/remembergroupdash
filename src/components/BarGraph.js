import React, { PureComponent } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

const data = [
  {
    name: "1",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "2",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "3",
    uv: 2000,
    pv: 9800,
    amt: 2290,
    fill: "#17BB6C",
  },
  {
    name: "4",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "5",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "6",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "7",
    uv: 3490,
    pv: 5000,
    amt: 2100,
  },
  {
    name: "8",
    uv: 3490,
    pv: 6000,
    amt: 2100,
  },
  {
    name: "9",
    uv: 3490,
    pv: 5600,
    amt: 2100,
  },
  {
    name: "10",
    uv: 3490,
    pv: 4200,
    amt: 2100,
  },
  {
    name: "11",
    uv: 3490,
    pv: 3500,
    amt: 2100,
  },
  {
    name: "12",
    uv: 3490,
    pv: 7400,
    amt: 2100,
  },
  {
    name: "13",
    uv: 3490,
    pv: 6200,
    amt: 2100,
  },
  {
    name: "14",
    uv: 3490,
    pv: 7400,
    amt: 2100,
  },
  {
    name: "15",
    uv: 3490,
    pv: 5800,
    amt: 2100,
  },
  {
    name: "16",
    uv: 3490,
    pv: 4400,
    amt: 2100,
  },
  {
    name: "17",
    uv: 3490,
    pv: 5500,
    amt: 2100,
  },
  {
    name: "18",
    uv: 3490,
    pv: 6500,
    amt: 2100,
  },
  {
    name: "19",
    uv: 3490,
    pv: 7300,
    amt: 2100,
  },
  {
    name: "20",
    uv: 3490,
    pv: 6300,
    amt: 2100,
  },
  {
    name: "21",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: "22",
    uv: 3490,
    pv: 5300,
    amt: 2100,
  },
  {
    name: "23",
    uv: 3490,
    pv: 8300,
    amt: 2100,
  },
  {
    name: "24",
    uv: 3490,
    pv: 7300,
    amt: 2100,
  },
  {
    name: "25",
    uv: 3490,
    pv: 6300,
    amt: 2100,
  },
  {
    name: "26",
    uv: 3490,
    pv: 5300,
    amt: 2100,
  },
  {
    name: "27",
    uv: 3490,
    pv: 3300,
    amt: 2100,
  },
  {
    name: "28",
    uv: 3490,
    pv: 8300,
    amt: 2100,
  },
  {
    name: "29",
    uv: 3490,
    pv: 6300,
    amt: 2100,
  },
  {
    name: "30",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: "31",
    uv: 3490,
    pv: 5300,
    amt: 2100,
  },
];

export default class BarGraph extends PureComponent {
  static demoUrl = "https://codesandbox.io/s/bar-chart-has-no-padding-jphoc";

  render() {
    return (
      <ResponsiveContainer width="100%" height={400}>
        <BarChart
          width={600}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 6,
            left: 6,
            bottom: 5,
          }}
          barSize={12}
        >
          <XAxis
            dataKey="name"
            /*scale="point"
            padding={{ left: 10, right: 10 }}*/
            axisLine={false}
            interval={0}
            tickLine={false}
          />
          <YAxis axisLine={false} tickLine={false} />
          <Tooltip />

          <Bar dataKey="pv" fill="#D6D6D6" radius={[4, 4, 4, 4]} />
        </BarChart>
      </ResponsiveContainer>
    );
  }
}
