import React, {useState} from "react";
import styled from "styled-components";
import searchIcon from '../assets/svgs/search-icon.svg'

const Container = styled.div`
  height: auto;
  width: 400px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;


const Title = styled.h1`
  font-weight: 600;
  font-size: 16px;
  color: #616161;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  height: 4em;
`;

const Label = styled.label`
  /*font-family: "Open Sans" !important;*/
  font-size: 12px;
  color: #616161;
  // font-weight: 600;
  margin-left: 15px;
  margin-bottom: 4px;
  display: inline;
  float: left;
`;

const ActionNotice = styled.p`
  cursor: pointer;
  font-size: 10px;
  text-align: center;
`

const SearchBox = styled.input`
  width: 356px;
  height: 44px;
  border: 1px solid;
  border-color: #c4c4c4;
  border-radius: 30px;
  font-size: 16px;
  padding-left: 22px;
  margin-bottom: 22px;
  background-color: white;
  ::placeholder {
    color: #cccccc;
  }
`;

const EditIcon = styled.i`
  float: right;
  color: #11B5E9;
  cursor: pointer;
  border: 1px solid #11B5E9;
  padding: 5px;
  border-radius: 50%;

`

const ButtonAddUser = styled.i`
  float: right;
  color: #11B5E9;
  cursor: pointer;

`

export default function UsersList(props) {

  const [showInactive, setShowInactive] = useState(false)


  const renderUsers = () => {
    return props.users.map(user => {
      let shows = showInactive || user.active
      if(shows){
        return (<Item>
          <Label>{user.email}</Label>
          <EditIcon  onClick={e => {
            e.preventDefault()
            props.openUser(user)
          }} className="material-icons">edit</EditIcon>
        </Item>)
      }
      return
    })
  }

  return (
    <Container>
      <Title>ACTIVE USERS 
        <span><ButtonAddUser className="material-icons" 
          onClick={e => {
          e.preventDefault()
          props.openNewUser()
      }}>add_circle</ButtonAddUser></span>
      
      </Title>
      <Divider />
      <Content>
          <Item>
              <SearchBox style={{
                 background: `url(${searchIcon}) no-repeat scroll 7px 7px`,
                 backgroundColor: 'white'
              }} type="text" />
          </Item>
        {renderUsers()}
      </Content>
      <ActionNotice onClick={() => {
        setShowInactive(!showInactive)
      }}>See inactive users</ActionNotice>
    </Container>
  );
}
