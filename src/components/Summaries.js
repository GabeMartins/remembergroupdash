import React from "react";
import styled from "styled-components";

const Container = styled.div`
  height: 173px;
  width: 376px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;

const Title = styled.h1`
  font-weight: 600;
  font-size: 16px;
  color: #616161;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-top: 12px;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  font-size: 40px;
  font-weight: 600;
  color: #232323;
  flex-direction: column;
`;

const Label = styled.span`
  /*font-family: "Open Sans" !important;*/
  font-size: 16px;
  margin-top: 4px;
`;

export default function Summaries({ pointsRedeemed, pointsAwarded }) {
  return (
    <Container>
      <Title>SUMMARIES</Title>
      <Divider />
      <Content>
        <Item>
          {pointsRedeemed}
          <Label>Points Redeemed</Label>
        </Item>
        <Item>
          {pointsAwarded}
          <Label>Points Awarded</Label>
        </Item>
      </Content>
    </Container>
  );
}
