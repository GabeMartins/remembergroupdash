import React from "react";
import styled from "styled-components";
import FindMembersTable from "./FindMembersTable";
import ReactSelect from "react-select";
import "../assets/css/select.css";
import IndicatingDots from "./IndicatingDots";

const Container = styled.div`
  height: auto;
  width: 400px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;

const Title = styled.h1`
  font-weight: 600;
  font-size: 40px;
  color: #11b5e9;
  margin-bottom: 12px;
  margin-left: 22px;
  margin-top: 22px;
`;

const Divider = styled.div`
  background: #d6d6d6;
  border: 0.5px solid #d6d6d6;
  margin-bottom: 22px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const ItemButton = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: right;
  align-items: flex-end;
  margin-top: 80px;
  margin-right: 22px;
`;

const Label = styled.label`
  /*font-family: "Open Sans" !important;*/
  font-size: 12px;
  color: #616161;
  font-weight: 600;
  margin-left: 15px;
  margin-bottom: 4px;
`;

const TextBox = styled.input`
  width: 356px;
  height: 44px;
  border: 1px solid;
  border-color: #c4c4c4;
  border-radius: 30px;
  font-size: 16px;
  background-color: #ffffff;
  padding-left: 22px;
  margin-bottom: 22px;
  ::placeholder {
    color: #cccccc;
  }
`;

const Button = styled.button`
  width: 179px;
  height: 44px;
  border-radius: 30px;
  background-color: #11b5e9;
  font-size: 20px;
  font-weight: 600;
  color: #ffffff;
  border: none;
  cursor: pointer;
`;

const Row = styled.div`
  display: flex;
  flex: 1;
`;


const options = [
  { value: "sales", label: "Sales" },
  { value: "services", label: "Services" },
  { value: "promotion", label: "Promotion" },
];

export default function ManagePointsFormThree() {
  return (
    <Container>
      <Title>Manage Points</Title>
      <Divider />
      <Content>
        <Item>
          <Label>RO</Label>
          <TextBox placeholder="#RO" type="text" />
        </Item>
        <Item>
          <Label>STOCK</Label>
          <TextBox type="text" placeholder="STOCK #" />
        </Item>
        <Item>
          <Label>SERVICE TYPE</Label>
          <ReactSelect className="rSelectCustom" options={options} />
        </Item>
      
        
    
       <Row>   
         <IndicatingDots mark="3/3" />
          <ItemButton>
            <Button>Search</Button>
          </ItemButton>
        </Row>
     
      </Content>
    </Container>
  );
}
