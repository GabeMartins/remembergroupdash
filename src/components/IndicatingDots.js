import React from "react";


export default function IndicatingDots(props) {

    const [index, total] = props.mark.split('/')
    const style = {
        fontSize: '80px',
        lineHeight: 0,
        color: '#C4C4C4'
    }

    const grayDot = () => <span style={style}>.</span>

    const blueDot = () => <span style={{
        ...style,
        color: '#11B5E9'
    }}>.</span>

    let dots = []
    for (let i = 0; i < total; i++) {
        if(i == index - 1){
            dots.push(blueDot())
        }else {
            dots.push(grayDot())
        }
        
    }

    return <p>{dots}</p>

}