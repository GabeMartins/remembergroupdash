import React from "react";
import styled from "styled-components";
import "../../assets/css/select.css";
import FirstStep from "./FirstStep";

const Container = styled.div`
  height: auto;
  width: 380px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;

const UpperHeader = styled.div`
  background-color: #EFEEEE;
  height: 2em;
  width: 424px;
  margin-top: 1em;
  margin-bottom: -2.6em;
  z-index: 1;
  position: relative;
  margin-left: 10px;
  border-radius: 25px 25px 0 0;
`

const Title = styled.p`
    text-align: center;
    padding-top: 0.5em
`

const ButtonClose = styled.span`
    float: right;
    margin-right: 2em;
    margin-top: -2em;
    cursor: pointer;
    color: #11B5E9;
    font-weight: bold;

`

export default function AddUserForm(props) {

  const title = () => {
    if (props.status == 'edit') {
      return 'Edit User'
    }

    return 'Add User'
  }

  const renderObj = () => {
    if (props.status == 'hide') {
      return (<div></div>)
    }

    return (
      <div>
        <UpperHeader>
            <div>
              <Title>{title()}</Title>
              <ButtonClose onClick={props.close}>X</ButtonClose>                
            </div>
        </UpperHeader>

        <Container style={{
            zIndex: 0,
          }}>
            <br/>
            <FirstStep {...props} />
        </Container>
      </div>
    )
  }

  return renderObj()
}
