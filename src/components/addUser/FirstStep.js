import React from "react";
import styled from "styled-components";
import "../../assets/css/select.css";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;


const Label = styled.label`
  /*font-family: "Open Sans" !important;*/
  font-size: 12px;
  color: #616161;
  font-weight: 600;
  margin-left: 15px;
  margin-bottom: 4px;
`;

const TextBox = styled.input`
  width: 356px;
  height: 44px;
  border: 1px solid;
  border-color: #c4c4c4;
  border-radius: 30px;
  font-size: 16px;
  background-color: #ffffff;
  padding-left: 22px;
  margin-bottom: 22px;
  ::placeholder {
    color: #cccccc;
  }
`;


const ItemButton = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: right;
  align-items: flex-end;
  margin-top: 80px;
  margin-right: 22px;
`;

const RightDiv = styled.div`
  width: 30%;
  height: 2em;
  margin-left: 70%;

`


const Button = styled.button`
  width: 179px;
  height: 44px;
  border-radius: 30px;
  background-color: #11b5e9;
  font-size: 20px;
  font-weight: 600;
  color: #ffffff;
  border: none;
  cursor: pointer;
`;

export default function FirstStep(props) {

  let user = props.user.user || {}
  return (
    <Content>
      <Item>
        <Label>FIRST NAME</Label>
        <TextBox type="text" value={user.name} />
      </Item>
      <Item>
        <Label>LAST NAME</Label>
        <TextBox type="text" />
      </Item>
      <Item>
        <Label>EMAIL</Label>
        <TextBox type="text" value={user.email} />
      </Item>
      <Item>
        <Label>CONFIRM EMAIL</Label>
        <TextBox type="text" value={user.email} />
      </Item>
      <Item>
        <RightDiv>
          <label for="military">MILITARY</label>
          <input style={{
            float: 'right'
          }} id="military" type="checkbox" /></RightDiv>
      </Item>

      <ItemButton>
        <Button>Next</Button>
      </ItemButton>
    </Content>
  );
}
