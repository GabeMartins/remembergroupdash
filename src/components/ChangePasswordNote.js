import React from "react";
import styled from "styled-components";

const Container = styled.div`
  height: 365px;
  width: 400px;
  background-color: #f6f8f9;
  border-radius: 25px;
  padding: 22px;
  /*font-family: Montserrat !important;*/
  margin: 10px;
`;
const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.p`
  /*font-family: "Open Sans" !important;*/
  font-size: 16px;
  margin-top: 4px;
  color: #000000;
  font-weight: 400;
  line-height: 30px;
`;

export default function ChangePasswordNote() {
  return (
    <Container>
      <Content>
        <Label>
          Use the form to pdate your login password. When confirming your new password, all characters entered must match exactly or the password will not update. If you have any questions or would like to alter other settings to your administrative account, please contact the re:member group at 952-224-8000.
        </Label>
      </Content>
    </Container>
  );
}
